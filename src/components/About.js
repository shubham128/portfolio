import React from "react"
import styles from "../styles/home.module.css"

const About = () => {
  return (
    <section className={styles.about_sec}>
      <h1 className={styles.home_heading}>About Me - </h1>
      <h3>Everything Start With a Passion for Technologies</h3>
      <p>
        I’m... a software engineer that loves the junction of engineering and
        design. I build experiences with exact attention to detail that comes
        through clean code and solid architecture. <br /> Can opt new challenges
        easily and ready to provide solution. Always willing to innovate the new
        things which can ease physical world problems using digitization. <br />
        <br />
        Have experience in <b>Web development</b>, <b>App development</b> and{" "}
        <b>Machine learning</b>.
      </p>
      <h2>Education:</h2>
      <h4>Bachelor of Science in Computer Science</h4>
      <h5>Rajiv Gandhi Prodyogiki Vishwavidyalaya</h5>
      <p>2013-2017</p>
      <h4>
        Schooling (6<sup>th</sup> - 12<sup>th</sup>)
      </h4>
      <h5>Jawahar Navodaya Vidyalaya</h5>
      <p>
        Completed 12<sup>th</sup> with computer science.
      </p>
      <h2>Experience:</h2>

      <h4>Senior Associate developer</h4>
      <h5>Infosys</h5>
      <span>Oct 2021 - Present</span>
      <p>
        Lead the team of web development. Find solution for development problems, solv queriesa and do development for web using react.js and material design.
      </p>

      <h4>Frontend developer</h4>
      <h5>Startxlabs</h5>
      <span>Oct 2019 - Sept 2021</span>
      <p>
        Frontend developer for web and mobile. Creatinf UI in HTML/CSS and react native for app. Website designing, Wordpress.
      </p>

      <h4>UI Designer and Web developer</h4>
      <h5>King Digital</h5>
      <span>Aug 2018 - Sept 2019</span>
      <p>
        Need to design website UI in Photoshop and convet it to code in HTML CSS
        JS (React/Angular) if required for Frontend development.
      </p>

      <h4>Website Designer and Wordpress developer</h4>
      <h5>Mindkey</h5>
      <span>Aug 2017 - Jan 2018</span>
      <p>
        My role was to create website design in HTML CSS JS as per designed in
        Photoshop and convert to wordpress theme.{" "}
      </p>




    </section>
  )
}

export default About
