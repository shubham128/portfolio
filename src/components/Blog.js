import React from "react";
import styles from "../styles/home.module.css";

const Blog = () => {
  return (
    <section className={styles.blog_sec}>
      <h1 className={styles.home_heading}>Blog - </h1>
      <div className={styles.blog_container}>
        <div className={styles.blog_listing}>
          <article className={styles.content_body}>
            <p>Website Design | Sept 25, 2019</p>
            <a
              target="_blank"
              href="https://medium.com/@kautilyshubham/gulp-js-for-website-development-a588c828bc44"
            >
              Gulp.js for website development incldes sass compiler, minify css,
              and many more.
            </a>
          </article>
        </div>

        <div className={styles.blog_listing}>
          <article className={styles.content_body}>
            <p>HTML CSS | Oct 29, 2019</p>
            <a
              target="_blank"
              href="https://codepen.io/kautilyshubham/post/how-to-write-good-css-for-website"
              className={styles.blog_detail_btn}
            >
              How to write good CSS for website to make performance better
              without rendering blockage.
            </a>
          </article>
        </div>
        <div className={styles.blog_listing}>
          <article className={styles.content_body}>
            <p>React Native | Jan 11, 2020</p>
            <a
              target="_blank"
              href="https://medium.com/@kautilyshubham/complete-react-native-setup-in-windows-a738dc9d86ac"
              className={styles.blog_detail_btn}
            >
              Complete guide in React Native Setup for Windows. Run in device
              and in android emulator.
            </a>
          </article>
        </div>
      </div>
    </section>
  );
};

export default Blog;
