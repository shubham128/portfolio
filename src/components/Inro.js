import React from "react";
import styles from "../styles/home.module.css";

const Intro = () => {
  return (
    <section className={styles.inro_sec}>
      <img
        src="/shubham-inro.png"
        alt="shubham pandey"
        className={styles.inro_img}
      />
    </section>
  );
};

export default Intro;
