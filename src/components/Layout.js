import React, { useState, useEffect } from "react";
import Sidebar from "./Sidebar";
import styles from "../styles/home.module.css";
import Intro from "./Inro";
import About from "./About";
import Project from "./Project";
import Skills from "./Skills";
import Blog from "./Blog";
import Footer from "./Footer";

// theme
import { ThemeProvider } from "styled-components";
import { lightTheme, darkTheme } from "../styles/theme";
import { GlobalStyles } from "../styles/global";

const Layout = () => {
  const [cTheme, setTheme] = useState("light");

  useEffect(() => {
    const th = localStorage.getItem("theme");
    if (th) {
      setTheme(th);
    } else {
      setTheme("light");
    }
  });

  const toggleTheme = () => {
    if (cTheme === "light") {
      setTheme("dark");
      localStorage.setItem("theme", "dark");
    } else {
      setTheme("light");
      localStorage.setItem("theme", "light");
    }
  };

  return (
    <ThemeProvider theme={cTheme === "light" ? lightTheme : darkTheme}>
      <>
        <GlobalStyles />
        <Sidebar changeTheme={toggleTheme} activeTheme={cTheme} />
        <div className={styles.content_part}>
          <Intro />
          <About />
          <Project />
          <Skills />
          <Blog />
          <Footer />
        </div>
      </>
    </ThemeProvider>
  );
};

export default Layout;
