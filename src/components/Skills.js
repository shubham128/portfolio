import React from "react";
import styles from "../styles/home.module.css";

const Skills = () => {
  return (
    <section className={styles.skill_sec}>
      <h1 className={styles.home_heading}>Skills - </h1>
      <div className={styles.skills_container}>
        <div class={styles.skill_box}>
          <h3>HTML</h3>
          <div className={styles.svg_box}>
            <span>100%</span>
            <svg class="progress-ring" height="150" width="150">
              <circle
                stroke="dodgerblue"
                stroke-width="5"
                fill="transparent"
                r="65"
                cx="75"
                cy="75"
              />
            </svg>
          </div>
          <p>3 years of exp.</p>
        </div>
        <div class={styles.skill_box}>
          <h3>CSS</h3>
          <div className={styles.svg_box}>
            <span>100%</span>
            <svg class="progress-ring" height="150" width="150">
              <circle
                stroke="dodgerblue"
                stroke-width="5"
                fill="transparent"
                r="65"
                cx="75"
                cy="75"
              />
            </svg>
          </div>
          <p>3 years of exp.</p>
        </div>
        <div class={styles.skill_box}>
          <h3>Bootstrap</h3>
          <div className={styles.svg_box}>
            <span>100%</span>
            <svg class="progress-ring" height="150" width="150">
              <circle
                stroke="dodgerblue"
                stroke-width="5"
                fill="transparent"
                r="65"
                cx="75"
                cy="75"
              />
            </svg>
          </div>
          <p>3 years of exp.</p>
        </div>
      </div>
      <div className={styles.horixontal_bar_container}>
        <div className={styles.horizontal_bar_box}>
          <h3>
            <b>Javascript</b>
            <span>80%</span>
          </h3>
          <div className={`${styles.h_bar} ${styles.p_80}`}></div>
        </div>
        <div className={styles.horizontal_bar_box}>
          <h3>
            <b>jQuery</b>
            <span>60%</span>
          </h3>
          <div className={`${styles.h_bar} ${styles.p_60}`}></div>
        </div>
        <div className={styles.horizontal_bar_box}>
          <h3>
            <b>React.js</b>
            <span>85%</span>
          </h3>
          <div className={`${styles.h_bar} ${styles.p_85}`}></div>
        </div>
        <div className={styles.horizontal_bar_box}>
          <h3>
            <b>Angular</b>
            <span>65%</span>
          </h3>
          <div className={`${styles.h_bar} ${styles.p_65}`}></div>
        </div>
        <div className={styles.horizontal_bar_box}>
          <h3>
            <b>React Native</b>
            <span>70%</span>
          </h3>
          <div className={`${styles.h_bar} ${styles.p_70}`}></div>
        </div>
        <div className={styles.horizontal_bar_box}>
          <h3>
            <b>Redux</b>
            <span>90%</span>
          </h3>
          <div className={`${styles.h_bar} ${styles.p_90}`}></div>
        </div>
        <div className={styles.horizontal_bar_box}>
          <h3>
            <b>WordPress</b>
            <span>90%</span>
          </h3>
          <div className={`${styles.h_bar} ${styles.p_90}`}></div>
        </div>
        <div className={styles.horizontal_bar_box}>
          <h3>
            <b>Photoshop </b>
            <span>75%</span>
          </h3>
          <div className={`${styles.h_bar} ${styles.p_75}`}></div>
        </div>
        <div className={styles.horizontal_bar_box}>
          <h3>
            <b>Figma, Adobe XD</b>
            <span>60%</span>
          </h3>
          <div className={`${styles.h_bar} ${styles.p_60}`}></div>
        </div>
        <div className={styles.horizontal_bar_box}>
          <h3>
            <b>Python</b>
            <span>60%</span>
          </h3>
          <div className={`${styles.h_bar} ${styles.p_60}`}></div>
        </div>
        <div className={styles.horizontal_bar_box}>
          <h3>
            <b>Data Science</b>
            <span>30%</span>
          </h3>
          <div className={`${styles.h_bar} ${styles.p_30}`}></div>
        </div>
      </div>
    </section>
  );
};

export default Skills;
