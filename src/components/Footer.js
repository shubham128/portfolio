import React from "react"

const Footer = () => {
  return (
    <footer>
      <h2>I'm Available for freelancing</h2>
      <h5>Deliverded more then 10 projects with good quality.</h5>
      <p>&copy; {new Date().getFullYear()}. Created by Shubham</p>
    </footer>
  )
}

export default Footer
