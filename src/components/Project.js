import React from "react";
import styles from "../styles/home.module.css";

const Project = () => {
  return (
    <section className={styles.project_sec}>
      <h1 className={styles.home_heading}>Projects - </h1>
      <div className={styles.project_list}>
        <div className={styles.project_left}>
          <img src="/social-app.png" alt="" />
        </div>
        <div className={styles.project_right}>
          <h3>Social Media Plateform</h3>
          <p>
            This is a social media plateform where user can register them self,
            create post, like and comment in posts and delete their post.
          </p>
          <p>
            <b>Created In-</b>
            React js, Node js and Firebase
          </p>
          <p>
            <b>URL:</b>{" "}
            <a
              href="https://react-social-media-ape.netlify.app/"
              target="_blank"
            >
              Visit Site
            </a>
          </p>
        </div>
      </div>
      <div className={styles.project_list}>
        <div className={styles.project_left}>
          <img src="/chat-app.png" alt="" />
        </div>
        <div className={styles.project_right}>
          <h3>Chat App</h3>
          <p>
            This is a chatting app where need to enter their name and room and
            start chatting in that room. Instant meessage will reflect in room.
          </p>
          <p>
            <b>Created In-</b>
            React js, Node js and Socket.io
          </p>
          <p>
            <b>URL:</b>{" "}
            <a href="https://react-socket-node-chat-app.netlify.app/">
              Visit Site
            </a>
          </p>
        </div>
      </div>
    </section>
  );
};

export default Project;
