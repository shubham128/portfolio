import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
  *,
  *::after,
  *::before {
    box-sizing: border-box;
  }

  body {
    align-items: center;
    background: ${({ theme }) => theme.body};
    color: ${({ theme }) => theme.text};
  }
  sidebar{
    background: ${({ theme }) => theme.sidebar};
  }
  section{
    background: ${({ theme }) => theme.section};
  }
  article{
    background: ${({ theme }) => theme.blog};
  }
  .home_skill_box__2-yE5{
    background: ${({ theme }) => theme.skillCard};
  }
  .home_h_bar__3hljB{
    background: ${({ theme }) => theme.skillBar};
  }
  footer{
    color: ${({ theme }) => theme.footerclr};
  }
  footer:before{
    background: ${({ theme }) => theme.footer};
  }
  `;
