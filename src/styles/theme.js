// theme.js
export const lightTheme = {
  body: "#fff",
  text: "#363537",
  link: "#1e90ff",
  sidebar: "#fafafa",
  section: "#cee1ff",
  footer: "#000000a8",
  footerclr: "#fff",
  skillCard: "#fff",
  skillBar: "#e6e6e6",
  blog: "#cee1ff",
};

export const darkTheme = {
  body: "#363537",
  text: "#6c757d",
  link: "#00d0ff",
  sidebar: "#1e1e30",
  section: "#161625",
  footer: "#0a2a58d9",
  footerclr: "#999",
  skillCard: "#1e1e30",
  skillBar: "#555",
  blog: "#161625",
};
